实现标签云（文字云）效果。划动屏幕，标签云还会缓缓移动。

    如何使用：

     1. 将CloudView类和CloudButton类的.h和.m文件导入你的工程中 

     2. 在你的类中导入CloudView的.h文件 #import “CloudView.h”

     3. 在你的类里面实现CloudView类的CloudViewDelegate 方法

     4. 参考如下实例化代码

     CloudView *cloud = [[CloudView alloc] initWithFrame:self.view.bounds

     andNodeCount:100];

     cloud.delegate = self;

     [self.view addSubview:cloud];

     [cloud release];

    开发者说：看到网上一些人在找有关标签云的代码，就写了一个小Demo。Demo不是很完美，为了触发事情将UILabel将为UIButton。此Demo只是抛砖引玉，期待更好的标签云代码。
